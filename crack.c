#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    if (hash == NULL || guess == NULL) return 0;
    
    // Hash the guess using MD5
	    
	char *guessHash = md5(guess, strlen(guess));
	    
	if (guessHash == NULL) return 0;

    // Compare the two hashes
    
    int result = 0;
    
    if (strcmp(guessHash, hash) == 0)
    {
        result = 1;
    }

    // Free any malloc'd memory
    
    free (guessHash);

    return result;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char* filename, int* size)
{
    *size = 0;
    if (filename == NULL) return NULL;
    
    // get total number of lines in the file
    int lineCount = 0;
    char line[1000];
    FILE* f = fopen(filename, "r");
    while (fgets(line, 1000, f) != NULL)
    {
        lineCount ++;
    }
    *size = lineCount;
    fclose(f);

    // allocate memory and get contents
    
    // each element of this array, e.g. words[i] is a pointer that points to a line of string
    char** words = malloc(lineCount * sizeof(char*));
    FILE* file = fopen(filename, "r");
    int n = 0;
    while (fgets(line, 1000, file) != NULL)
    {
        int lineLength = strlen(line);
        if (line[lineLength - 1] == '\n')
        {
            line[lineLength - 1] = '\0';
        }
        char* word = malloc(strlen(line) * sizeof(char) + 1);
        strcpy(word, line);
        words[n] = word;
        n++;
    }
    fclose(file);
    
    return words;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the password dictionary file into an array of strings.
    int dLines = 0;
    char **dict = read_dictionary(argv[2], &dLines);

    // Open the hash file for reading.
    FILE* hashes = fopen(argv[1], "r");

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    char hash[1000];
    while (fgets(hash, 1000, hashes) != NULL)
    {
        int hashLength = strlen(hash);
        if (hash[hashLength - 1] == '\n')
        {
            hash[hashLength - 1] = '\0';
        }
        
        for (int i = 0; i < dLines; i++)
        {
            if (tryguess(hash, dict[i]) == 1)
            {
                // a match is found
                printf("Hash = %s, password = %s\n", hash, dict[i]);
                break;
            }
        }
    }
    
    fclose(hashes);
    
    // free allocated memory
    
    for (int i = 0; i < dLines; i++)
    {
        free(dict[i]);
    }
    
    free(dict);
}
